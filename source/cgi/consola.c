#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "parser.h"

#define SIZE 256

int main(int argC, char *argV[]) {
	char *file;
	file = "datos_meteorologicos.CSV";

	if (argC > 1) {
		if (!strcmp(argV[1], "listar")) {
				char *stations[10], *names[10];
				int i, qty;

				qty = get_stations(file, stations, names);
				for (i = 0; i < qty; i++) {
					printf("%s:  %s\n", stations[i], names[i]);
				}
		} else if (!strcmp(argV[1], "diario_precipitacion")) {
			if (argC == 3) {
				char *days[31];
				double *averages[31];
				int qty, j;

				qty = get_station_daily_precipitation(file, argV[2], days, averages);

				for (j = 0; j < qty; j++) {
					printf("%s: %.2f mm\n", days[j], *averages[j]);
				}
			} else {
				printf("Uso: diario_precipitacion nro_estacion\n");
			}
		} else if (!strcmp(argV[1], "mensual_precipitacion")) {
			if (argC == 3) {
				char *months[12];
				double *averages[31];
				int qty, j;

				qty = get_station_monthly_precipitation(file, argV[2], months, averages);

				for (j = 0; j < qty; j++) {
					printf("%s: %.2f mm\n", months[j], *averages[j]);
				}
			} else {
				printf("Uso: mensual_precipitacion nro_estacion\n");
			}
		} else if (!strcmp(argV[1], "promedio")) {
			char *vars[20];
			int vars_qty;
			vars_qty = get_headers(file, vars);

			if (argC == 3) {
				long ret;

				ret = strtol(argV[2], NULL, 10);

				if (ret >= 4 && ret < vars_qty) {
					char *stations[10], *names[10];
					double *averages[20];
					int qty, j;

					qty = get_stations(file, stations, names);
					printf("Variable: %s\n", vars[ret]);

					for (j = 0; j < qty; j++) {
						get_station_averages(file, stations[j], averages);
						printf("%s: %.2f\n", stations[j], *averages[ret]);
					}
				} else {
					int j;
					for (j = 4; j < vars_qty; j++) {
						printf("%d: %s\n", j, vars[j]);
					}
				}
			} else {
				int j;
				for (j = 4; j < vars_qty; j++) {
					printf("%d: %s\n", j, vars[j]);
				}
			}
		} else if (!strcmp(argV[1], "descargar")) {
			if (argC == 3) {
				char *archivo;
				archivo = make_station_file(file, argV[2]);
				printf("Archivo: %s\n", archivo);
			} else {
				printf("Uso: descargar nro_estacion\n");
			}
		}
	} else {
		printf("Uso: consola comando\n");
	}
	return 0;
}