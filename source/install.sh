#!/bin/bash
apt-get update

apt-get install lighttpd bc

wget https://raw.githubusercontent.com/notro/rpi-source/master/rpi-source -O /usr/bin/rpi-source && sudo chmod +x /usr/bin/rpi-source && /usr/bin/rpi-source -q --tag-update

rpi-source

cp -r cgi/* /usr/lib/cgi-bin/
mkdir /usr/lib/cgi-bin/tmp
chmod 0777 /usr/lib/cgi-bin/tmp

cp 010_www-data-mod /etc/sudoers.d/
cp cgi.conf /etc/lighttpd/conf-enabled/10-cgi.conf
service lighttpd restart

cp -r www/* /var/www/html/
ln -s /usr/lib/cgi-bin/tmp /var/www/html/tmp

cd /usr/lib/cgi-bin/
make
chmod 0655 consola info exec.pl upload.pl
chmod a+x module