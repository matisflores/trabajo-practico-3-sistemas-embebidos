var base = '/';

$(document).ready(function(){
	//bindings
	WebApp._bind("Router.change", function(){
		$('#container').addClass('panel-default');
		$('#container').removeClass('panel-danger');
		$('#container').hide();
	});
	WebApp._bind("Parser.parsed", function(){
		$('#container').show(1000);
	});
	WebApp._bind("Router.error", function(){
		error('Sección no encontrada');
		$('#container').show(1000);
	});
});

function error(msg) {
	$('#container').removeClass('panel-default');
	$('#container').addClass('panel-danger');
	$('#title').text('Error');
	$('#wrapper').text(msg);
}

function home() {
	$('#title').html('Trabajo Pr&aacute;ctico 3 - Sistemas Embebidos');
	WebApp._parse('home');
}

function info() {
	$('#title').html('Informaci&oacute;n del Sistema');
	execCGI({ cmd: 'info' }, function(data){
		WebApp._parse('info', null, data);
	});	
}

function estaciones() {
	$('#title').html('Estaciones Meteorol&oacute;gicas');
	execCGI({ cmd: 'consola', param1: 'listar' }, function(data) {
		WebApp._parse('estaciones', null, data);
	});	
}

function descargar(params) {
	if(params.id) {
		execCGI({ cmd: 'consola', param1: 'descargar', param2: params.id }, function(data) {
			$('#download_iframe').attr('src', base + data.Archivo);
			WebApp.Router._navigate('estaciones');
		});
	}
}

function diario_precipitacion(params) {
	if(params.id) {
		$('#title').html('Precipitaci&oacute;n Diaria: ' + params.id);
		execCGI({ cmd: 'consola', param1: 'diario_precipitacion', param2: params.id }, function(data) {
			WebApp._parse('precipitacion', null, data);
		});
	}
}

function mensual_precipitacion(params) {
	if(params.id) {
		$('#title').html('Precipitaci&oacute;n Mensual: ' + params.id);
		execCGI({ cmd: 'consola', param1: 'mensual_precipitacion', param2: params.id }, function(data) {
			WebApp._parse('precipitacion', null, data);
		});
	}
}

function promedio() {
	$('#title').html('Promedios Disponibles');
	execCGI({ cmd: 'consola', param1: 'promedio' }, function(data) {
		WebApp._parse('promedio', null, data);
	});	
}

function promedio_var(params) {
	if(params.id) {
		execCGI({ cmd: 'consola', param1: 'promedio', param2: params.id }, function(data) {
			$('#title').html(data.Variable);
			data.keys.shift();
			WebApp._parse('promedio_var', null, data);
		});
	} else {
		WebApp.Router._navigate('promedio');
	}
}

/* MODULOS */
var fille_name = '';
function modulos() {
	$('#title').html('M&oacute;dulos Cargados');
	execCGI({ cmd: 'lsmod' }, function(data) {
		WebApp._bind('show_form', function(){
			$('#container').show(1000);
			$('#file').on('change', function() {
			    var file = this.files[0];
			    if (file.size > 1024 * 5) {
			        alert('Tamaño máximo es 5k');
			    }

			    if( /.ko$/.test(file.name) == false) {
			    	alert('Solo archivos .ko');
			    }
			    file_name = '/tmp/' + file.name;
			});

			$('#upload').on('click', function() {
			    $.ajax({
			        url: base + 'cgi-bin/upload.pl',
			        type: 'POST',
			        data: new FormData($('form')[0]),
			        cache: false,
			        contentType: false,
			        processData: false,
			        complete: instalar_modulo
			    });
			});
		});

		WebApp._parse('modulos', null, data, 'show_form');
	});	
}

function instalar_modulo() {
	execCGI({ cmd: 'sudo', param1: 'insmod', param2: file_name }, function(data) {
		modulos();
	});	
}

function desinstalar_modulo() {
	execCGI({ cmd: 'sudo', param1: 'rmmod', param2: 'hello' }, function(data) {
		modulos();
	});	
}

function descargar_modulo() {
	$('#download_iframe').attr('src', base + 'tmp/hello.ko');
	WebApp.Router._navigate('modulos');
}

/* GENERALES */

function execCGI(data, callback) {
	$.get(base + 'cgi-bin/exec.pl', data, function(data){
		var result = {
			keys : []
		};
		var parts, key;
		$.each(data.split(/\n/), function(i, line){
	        if(line){
	            //console.log(line);
	            key = line.substr(0, line.indexOf(':'));
	            if(key != '') {
	            	result[key] = line.substr(line.indexOf(':') + 2);
	            	result.keys.push(key);
	            } else {
	            	if (result.data == null)
	            		result.data = [];
	            	result.data.push(line);
	        	}
	        }
	    });
	    callback(result);
	},'text').fail(callback);
}