{
	routes : {
		home : '',
		info : 'info',
		estaciones : 'estaciones',
		descargar : 'descargar:id',
		diario_precipitacion : 'diario_precipitacion:id',
		mensual_precipitacion : 'mensual_precipitacion:id',
		promedio : 'promedio',
		promedio_var : 'promedio:id',
		modulos : 'modulos'
	}
}